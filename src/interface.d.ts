interface TypeEventI<TParam> {
    add<TChildParam>(name?:string): TypeEventI<TChildParam>
    on(handler: Callback<TParam>): TypeEventI<TParam>
    once(handler: Callback<TParam>): TypeEventI<TParam>
    trigger(param?: TParam): TypeEventI<TParam>
    off(handler?: Callback<TParam>): TypeEventI<TParam>
    listenTo<TSourceParam>(source: TypeEventI<TSourceParam>, handler: Callback<TSourceParam>): TypeEventI<TParam>
    listenToOnce<TSourceParam>(source: TypeEventI<TSourceParam>, handler: Callback<TSourceParam>): TypeEventI<TParam>
    stopListening<TSourceParam>(source?: TypeEventI<TSourceParam>, callback?: Callback<TSourceParam>): TypeEventI<TParam>
}

interface Listening<TParam> {
    source: TypeEventI<TParam>;
    handler: Callback<TParam>;
}

interface Handler<TParam> {
    callback: Callback<TParam>;
    once: boolean;
    listener: TypeEventI<any>;
}

interface Callback<TParam> {
    (names: string[], param: TParam): void;
}

declare module SInput {

    interface Options {
        placeholder?:string;
    }

    interface IKeyDownEvent {
        text: string;
        event: KeyboardEvent;
    }

    interface ItemData {
        toDisplay: string;
        toAction: any;
    }

    interface IMainEvents {
        change:TypeEventI<string>;
        focus: TypeEventI<{}>;
        blur: TypeEventI<{}>;
        enter: TypeEventI<string>;
        keyDown: TypeEventI<IKeyDownEvent>
        select: TypeEventI<ItemData>
    }

    interface Input {
        destroy();
        val(text:string):void;
        getText():string;
    }

    interface Main {
        events: IMainEvents;
        destroy():void;
        getText():string;
        setList(data:Array<ItemData>):void;
        getActive():ItemData;
        setSearchText(text:string):void;
    }

    interface List {
        render(data:Array<ItemData>):void;
        destroy():void;
        getActiveItem():ItemData;
    }

    interface Item {
        destroy():void;
        enable():void;
        disable():void;
        getData():ItemData;
    }
}