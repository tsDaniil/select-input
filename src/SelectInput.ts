/// <reference path="./interface.d.ts" />
/// <reference path="./components/Input.ts" />
/// <reference path="./components/List.ts" />
/// <reference path="./TypeEvent.ts" />

class SelectInput implements SInput.Main {

    public events:SInput.IMainEvents;

    private options:SInput.Options;
    private node:HTMLElement;
    private input:SInput.Input;
    private list:SInput.List;

    constructor(node:HTMLElement, options:SInput.Options) {
        this.options = options || {};
        this.node = node;
        this.initialize();
    }

    public destroy():void {
        this.input.destroy();
        this.list.destroy();
        Input.forEach(this.events, (event:TypeEventI<any>) => {
            event.off();
        });
    }

    public getText():string {
        return this.input.getText();
    }

    public setList(data:Array<SInput.ItemData>):void {
        this.list.render(data);
    }

    public getActive():SInput.ItemData {
        return this.list.getActiveItem();
    }

    public setSearchText(text:string):void {
        this.input.val(text);
    }

    private initialize():void {
        this.setHandlers();
        this.createInput();
        this.createList();
    }

    private createInput():void {
        this.input = new Input(this.node, this, this.options);
    }

    private createList():void {
        this.list = new List(this.node, this);
    }

    private setHandlers():void {

        this.createEvents();

        this.events.keyDown.on((names, data:SInput.IKeyDownEvent) => {

            if (SelectInput.keyCodesToPreventDefault.indexOf(data.event.keyCode) != -1) {
                data.event.preventDefault();
                data.event.stopPropagation();
            }

        });
    }

    private createEvents():void {
        this.events = {
            change: new TypeEvent(),
            focus: new TypeEvent(),
            blur: new TypeEvent(),
            enter: new TypeEvent(),
            keyDown: new TypeEvent(),
            select: new TypeEvent()
        };
    }

    private static keyCodesToPreventDefault = [
        13, 27, 38, 40, 37, 39
    ];

}