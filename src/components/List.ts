/// <reference path="../interface.d.ts" />
/// <reference path="./Item.ts" />

class List implements SInput.List {

    private parentNode:HTMLElement;
    private parent:SInput.Main;
    private node:HTMLElement;
    private collection: Array<SInput.Item> = [];
    private active:number = 0;

    constructor(node:HTMLElement, parent:SInput.Main) {
        this.parentNode = node;
        this.parent = parent;
        this.initialize();
    }

    public getActiveItem():SInput.ItemData {
        return (this.has(this.active) ? this.collection[this.active].getData() : null);
    }

    public render(data:Array<SInput.ItemData>):void {
        this.removeCollection();
        this.createCollection(data);
    }

    public destroy():void {
        this.removeCollection();
    }

    private initialize():void {
        this.createDom();
        this.setHandlers();
    }

    private setHandlers():void {
        this.parent.events.keyDown.on((names, data:SInput.IKeyDownEvent) => {
            if (data.event.keyCode in List.keyCodeMap) {
                this[List.keyCodeMap[data.event.keyCode]]();
            }
        });
    }

    private has(index):boolean {
        return index in this.collection;
    }

    private up():void {
        this.setActive(this.active - 1);
    }

    private down():void {
        this.setActive(this.active + 1);
    }

    private setActive(index:number):void {
        if (this.has(index)) {
            this.getActive().disable();
            this.active = index;
            this.getActive().enable();
        }
    }

    private getActive():SInput.Item {
        return this.collection[this.active];
    }

    private createDom():void {
        this.node = <HTMLElement>document.createElement('UL');
        this.node.className = 'select-list';
        this.parentNode.appendChild(this.node);
    }

    private removeCollection():void {
        this.collection.forEach((item:SInput.Item) => {
            item.destroy();
        });
        this.collection = [];
        this.active = 0;
    }

    private createCollection(data:Array<SInput.ItemData>):void {
        data.forEach((itemData:SInput.ItemData) => {
            this.collection.push(new Item(this.node, itemData, this.parent));
        });
        if (this.has(this.active)) {
            this.getActive().enable();
        }
    }

    private static keyCodeMap = {
        38: 'up',
        40: 'down'
    };

}