/// <reference path="../interface.d.ts" />

class Item implements SInput.Item {

    private node:HTMLElement;
    private parentNode:HTMLElement;
    private parent:SInput.Main;
    private data:SInput.ItemData;
    private clickHandler:()=>void;

    constructor(parentNode:HTMLElement, data:SInput.ItemData, parent:SInput.Main) {
        this.parentNode = parentNode;
        this.parent = parent;
        this.data = data;
        this.initialize();
    }

    public destroy():void {
        this.dropHandlers();
        this.parentNode.removeChild(this.node);
    }

    public enable():void {
        this.node.classList.add('active');
    }

    public disable():void {
        this.node.classList.remove('active');
    }

    public getData():SInput.ItemData {
        return this.data;
    }


    private initialize():void {
        this.createDom();
        this.setHandlers();
    }

    private createDom():void {
        this.node = <HTMLElement>document.createElement('LI');
        this.node.className = 's-list-item';
        this.node.innerHTML = this.data.toDisplay;
        this.parentNode.appendChild(this.node);
    }

    private setHandlers():void {
        this.clickHandler = () => {
            this.parent.events.select.trigger(this.getData());
        };
        this.node.addEventListener('mousedown', this.clickHandler, false);
    }

    private dropHandlers():void {
        this.node.removeEventListener('mousedown', this.clickHandler, false);
    }

}