/// <reference path="../interface.d.ts" />

class Input implements SInput.Input {

    private parentNode:HTMLElement;
    private node:HTMLInputElement;
    private parent:SInput.Main;
    private handlers:IHandlers;
    private options: SInput.Options;

    constructor(node:HTMLElement, parent:SInput.Main, options:SInput.Options) {
        this.options = options;
        this.parentNode = node;
        this.parent = parent;
        this.initialize();
    }

    public destroy():void {
        this.dropHandlers();
    }

    public val(text:string):void {
        this.node.value = text;
    }

    public getText():string {
        return this.node.value;
    }

    private initialize():void {
        this.createNode();
        this.setHandlers();
    }

    private createNode():void {
        this.node = <HTMLInputElement>document.createElement('INPUT');
        this.node.type = 'text';
        this.node.className = 's-input';
        this.node.placeholder = this.options.placeholder || '';
        this.parentNode.appendChild(this.node);
    }

    private setHandlers():void {
        this.handlers = {
            focus: () => {
                this.focus();
            },
            blur: () => {
                this.blur();
            },
            input: () => {
                this.input(this.node.value);
            },
            keydown: (event:KeyboardEvent) => {
                this.keydown(this.node.value, event);
                if (event.keyCode == 13) {
                    this.parent.events.enter.trigger(this.node.value);
                }
            }
        };
        Input.forEach(this.handlers, (handler, name) => {
            this.node.addEventListener(name, handler, false);
        });
    }

    private focus():void {
        this.parent.events.focus.trigger();
    }

    private blur():void {
        this.parent.events.blur.trigger();
    }

    private input(text:string):void {
        this.parent.events.change.trigger(text);
    }

    private keydown(text:string, event:KeyboardEvent):void {
        this.parent.events.keyDown.trigger({
            text: text,
            event: event
        });
    }

    private dropHandlers():void {
        Input.forEach(this.handlers, (handler, name) => {
            this.node.removeEventListener(name, handler, false);
        });
    }

    public static forEach(object, callback:(value:any, key:string)=>void):void {
        for (var key in object) {
            if (object.hasOwnProperty(key)) {
                callback(object[key], key);
            }
        }
    }

}

interface IHandlers {
    focus:() => void;
    blur:() => void;
    input:() => void;
    keydown:(event:KeyboardEvent) => void;
}