/// <reference path="./interface.d.ts" />

class TypeEvent<TParam> implements TypeEventI<TParam> {

    private parent: TypeEvent<any>;
    private children: TypeEvent<any>[] = [];
    private handlers: Array<Handler<TParam>> = [];
    private listenings: Array<Listening<any>> = [];
    private name:string = "";

    constructor(name?:string) {
        this.name = name;
    }

    public add<TChildParam>(name?:string): TypeEvent<TChildParam> {
        var child: TypeEvent<TChildParam> = new TypeEvent<TChildParam>(name);
        this.children.push(child);
        child.parent = this;
        return child;
    }

    public on(handler: Callback<TParam>): TypeEvent<TParam> {
        this.addHandler(handler);
        return this;
    }

    public once(handler: Callback<TParam>): TypeEvent<TParam> {
        this.addHandler(handler, true);
        return this;
    }

    public trigger(param?: TParam): TypeEvent<TParam> {
        this._trigger([], param);
        return this;
    }

    public off(handler?: Callback<TParam>): TypeEvent<TParam> {
        this.handlers = this.handlers.filter((someHandler: Handler<TParam>) => {
            var listener: any = someHandler.listener;

            if (TypeEvent.isSameOrFalsy(someHandler.callback, handler)) {
                if (listener) {
                    listener.removeListening(this, someHandler.callback)
                }

                return false;
            }

            return true;
        });

        this.children.forEach((child: TypeEvent<TParam>) => {
            child.off();
        });

        return this;
    }

    public listenTo<TSourceParam>(source: TypeEvent<TSourceParam>, handler: Callback<TSourceParam>): TypeEvent<TParam> {
        source.addHandler(handler, false, this);
        this.listenings.push({
            source: source,
            handler: handler
        });
        return this;
    }

    public listenToOnce<TSourceParam>(source: TypeEvent<TSourceParam>, handler: Callback<TSourceParam>): TypeEvent<TParam> {
        source.addHandler(handler, true, this);
        this.listenings.push({
            source: source,
            handler: handler
        });
        return this;
    }

    public stopListening<TSourceParam>(source?: TypeEvent<TSourceParam>, callback?: Callback<TSourceParam>): TypeEvent<TParam> {
        this.listenings.forEach((listening: Listening<TSourceParam>) => {
            if (TypeEvent.isSameOrFalsy(listening.source, source) && TypeEvent.isSameOrFalsy(listening.handler, callback)) {
                listening.source.off(listening.handler);
                return false;
            }

            return true;
        });

        if (source) {
            source.children.forEach((child: TypeEvent<any>) => {
                this.stopListening(child, callback);
            });
        }

        this.children.forEach((child: TypeEvent<any>) => {
            child.stopListening(source, callback);
        });

        return this;
    }

    private addHandler(callback: Callback<TParam>, once: boolean = false, listener?: TypeEvent<any>) {
        this.handlers.push({
            callback: callback,
            once: once,
            listener: listener
        });
    }

    private removeListening<TSourceParam>(source: TypeEvent<TSourceParam>, handler: Callback<TSourceParam>) {
        this.listenings.filter((listening: Listening<TSourceParam>) => {
            return listening.handler != handler || listening.source != source;
        });
    }

    private _trigger(names:string[], param?:TParam):void {

        this.handlers.forEach((handler: Handler<TParam>) => {
            handler.callback(names, param);
            if (handler.once) {
                this.off(handler.callback)
            }
        });

        names.push(this.name);

        if (this.parent) {
            this.parent._trigger(names, param);
        }
    }

    private static isSameOrFalsy(staff, same_or_falsy) {
        return !same_or_falsy || staff == same_or_falsy;
    }
}