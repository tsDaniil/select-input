/// <reference path="./typings/tsd.d.ts" />

import gulp = require('gulp');
import tsc = require('gulp-typescript');
import fs = require('fs');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var concat = require('gulp-concat');

gulp.task('debug', function  () {
    return gulp.src(['src/*/*.ts', 'src/*.ts'])
        .pipe(tsc({}))
        .pipe(concat('sInput.all.js'))
        .pipe(gulp.dest('./build/'));
});

gulp.task('default', ['debug'], function () {
    return gulp.src('./build/sInput.all.js')
        .pipe(uglify())
        .pipe(rename('sInput.min.js'))
        .pipe(gulp.dest('./build/'));
});

gulp.task('common', ['default'], function () {

    fs.writeFileSync('build/sInput.amd.min.js',
        fs.readFileSync('./build/sInput.min.js', 'utf8') + 'module.exports = SelectInput;');

});